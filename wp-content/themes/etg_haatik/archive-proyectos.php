<?php get_header(); ?>

<article id="contenido">
    <div class="container">
        <div class="row">
        <?php 
        if ( have_posts() ) { 
            query_posts( array('post_type' => 'proyectos','orderby' => 'menu_order', 'order' => 'ASC') );
            while ( have_posts() ) { 
                the_post();
            ?>
                <div class="col-md-4 col-sm-6">
                    <div class="proyecto">
                    <?php 
                    if(has_post_thumbnail($the_query->ID)){ ?>
                        <p class="foto">
                            <a href="<?php echo the_permalink(); ?>"><?php the_post_thumbnail('poster', array('class' => 'img-responsive')); ?></a>
                        </p>
                    <?php 
                    } else { ?>
                        <a href="<?php echo the_permalink(); ?>"><img src="http://eltipografico.com/_herramientas/dummyimage/420x620/999/fff&text=" alt="TEST" class="img-responsive" /></a>
                    <?php }
                    ?>                    
<!--
            		<div class="info">
                        <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                    </div>
-->
                    </div>
                </div><!-- .col-md-4 -->
            <?php 
            } 
        } wp_reset_postdata(); ?>
    </div><!-- row -->
    </div>
</article>
	
<?php get_footer(); ?> 