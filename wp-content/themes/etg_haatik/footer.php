        <footer id="pie">
            <div class="container">
            	<div class="row">
            	    <div class="col-md-3">
            	    	<img src="<?php bloginfo( 'template_url' ); ?>/img/contact.png" alt="contact" />
            	    	<p>
                	    	<span class="text-uppercase"><strong>Bulegoa</strong></span><br />
                	    	<a href="mailto:haatik@haatik.com">haatik@haatik.com</a><br />
                	    	943 51 82 99
                	   </p>
            	    </div><!-- .col-md-3 -->
            	    <div class="col-md-3">
            	    	<p>
                	    	<span class="text-uppercase"><strong>Zuzendaritza</strong></span><br />
                	    	<a href="mailto:aiert@beobide.net">aiert@beobide.net</a><br />
                	    	609 10 10 75
                	   </p>
            	    	<p>
                	    	<span class="text-uppercase"><strong>Komunikazioa</strong></span><br />
                	    	<a href="mailto:haatik@haatik.com">iurre@haatik.com</a><br />
                	    	609 10 19 59
                	   </p>
            	    </div><!-- .col-md-3 -->
            	    <div class="col-md-2">

            	    	<a href="https://www.instagram.com/haatikdantzakonpainia" class="social instagram">Instagram</a>
            	    	<a href="https://www.vimeo.com/haatikdantzakonpainia" class="social vimeo">Vimeo</a>
            	    	<a href="https://www.facebook.com/HaatikDantzaKonpainia" class="social fb">Like</a>
            	    	<a href="https://twitter.com/haatik_dantza" class="social tw">Tweet</a>
            	    </div><!-- .col-md-2 -->
            	    <div class="col-md-4 text-right">
            	    	<img src="<?php bloginfo( 'template_url' ); ?>/img/logos-pie.png" alt="logos-pie" />
            	    </div><!-- .col-md-4 -->
            	</div><!-- .row -->
            </div><!-- .container -->
        </footer>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> -->
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?php bloginfo('template_url');?>/js/bootstrap.min.js"></script>
        <script src="<?php bloginfo('template_url');?>/js/main.js"></script>
        
        <script src="<?php bloginfo( 'template_url' ); ?>/js/mp.mansory.js"></script>
        <script type="text/javascript">
            $(".galeria-masonry").mpmansory({
                breakpoints: {
                    lg: 4,
                    md: 4,
                    sm: 6,
                    xs: 12
                }
            });
        </script>        
        
        <?php wp_footer(); ?>
    </body>
</html>







<!-- end -->