<?php
    
function set_idiomas() {
    global $idioma, $locale,$fecha;
    
    $fecha = date('Y-m-d');
    $idioma = ICL_LANGUAGE_CODE;
	switch ($idioma){
		case "es":
            define('MY_LOCALE', 'es_ES.UTF-8');
            define('IDIOMA_AGENDA', 'es');
            define('IDIOMA_NOTICIAS', 'es');
		break;
		case "eu":
            define('MY_LOCALE', 'eu_ES.UTF-8');
            define('IDIOMA_AGENDA', 'eu');
            define('IDIOMA_NOTICIAS', 'eu');
		break;
		case "en":
            define('MY_LOCALE', 'en_EN.UTF-8');
            define('IDIOMA_AGENDA', 'en');
            define('IDIOMA_NOTICIAS', 'es');
		break;
		case "fr":
            define('MY_LOCALE', 'fr_FR.UTF-8');
            define('IDIOMA_AGENDA', 'fr');
            define('IDIOMA_NOTICIAS', 'fr');
		break;
		default:
            define('MY_LOCALE', 'eu_ES.UTF-8');
            define('IDIOMA_AGENDA', 'eu');
            define('IDIOMA_NOTICIAS', 'eu');
		break;
	}
	setlocale(LC_TIME, MY_LOCALE);
    $idioma = ICL_LANGUAGE_CODE;
}
add_action( 'init', 'set_idiomas' ); 

add_theme_support( 'post-thumbnails' );
set_post_thumbnail_size( 360, 360, true );
#add_image_size( 'miniatura', 360, 360, true );
add_image_size( 'poster', 420, 620, true );
add_image_size( 'noticias', 555, 210, true );
add_image_size( 'miniatura-grande', 300, 300, true );
add_image_size( 'slide', 1160, 450, true );
add_image_size( 'slide-grande', 2000, 2000);




function custom_excerpt_length( $length ) {
	return 20;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );



add_filter( 'embed_oembed_html', 'custom_oembed_filter', 10, 4 ) ;
function custom_oembed_filter($html, $url, $attr, $post_ID) {
    $return = '<div class="video-container">'.$html.'</div>';
    return $return;
}

function ETG_categorias ($id){
    if (get_post_type($id) == "agenda"){
        $tipo = get_the_terms($id, 'tipo');
        if(get_the_terms($id, 'tipo')){
            foreach($tipo as $c) {
            	$cat = get_category($c);
            	#print_r($cat);
            	$li.='<li><a href="'.get_category_link($cat->term_id).'" class="text-uppercase">'.$cat->cat_name.'</a></li>';
            }
        }
        $programa = get_the_terms($id, 'programa');
        if (get_the_terms($id, 'programa')){
        foreach($programa as $c) {
        	$cat = get_category($c);
        	#print_r($cat);
        	$li.='<li><a href="'.get_category_link($cat->term_id).'" class="text-uppercase">'.$cat->cat_name.'</a></li>';
        }
        }
    } else {
        $cont = wp_get_post_categories($id);
        foreach($cont as $c) {
        	$cat = get_category($c);
        	#print_r($cat);
        	$li.='<li><a href="'.get_category_link($cat->term_id).'" class="text-uppercase">'.$cat->cat_name.'</a></li>';
        }
    
    }
    
    return $li;
}





    
# Soporte idiomas     
load_theme_textdomain('ETG_text_domain', get_template_directory() . '/languages');



// ******************* Sidebars ****************** //

if ( function_exists('register_sidebar') ) {
	register_sidebar(array(
		'name' => 'Pages',
		'before_widget' => '<li id="%1$s" class="widget %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h2>',
		'after_title' => '</h2>',
	));
}

// ******************* Add Custom Menus ****************** //

add_theme_support( 'menus' );


// ******************* Post Thumbnails ****************** //


/*
    CUSTOM POST TYPE    
*/
$labels = array(
	'name'               => _x( 'Proyectos', 'post type general name', 'ETG_text_domain' ),
	'singular_name'      => _x( 'Proyecto', 'post type singular name', 'ETG_text_domain' ),
	'menu_name'          => _x( 'Proyectos', 'admin menu', 'ETG_text_domain' ),
	'name_admin_bar'     => _x( 'Proyecto', 'add new on admin bar', 'ETG_text_domain' ),
	'add_new'            => _x( 'Agregar nuevo', 'book', 'ETG_text_domain' ),
	'add_new_item'       => __( 'Agregar nuevo proyecto', 'ETG_text_domain' ),
	'new_item'           => __( 'Nuevo proyecto', 'ETG_text_domain' ),
	'edit_item'          => __( 'Editar proyecto', 'ETG_text_domain' ),
	'view_item'          => __( 'Ver proyecto', 'ETG_text_domain' ),
	'all_items'          => __( 'Todos los proyectos', 'ETG_text_domain' ),
	'search_items'       => __( 'Buscar proyectos', 'ETG_text_domain' ),
	'parent_item_colon'  => __( 'Proyectos hijo:', 'ETG_text_domain' ),
	'not_found'          => __( 'No se encontraron proyectos.', 'ETG_text_domain' ),
	'not_found_in_trash' => __( 'No se encontraron proyectos en la papelera.', 'ETG_text_domain' )
);



register_post_type('proyectos', array(
	'labels' => $labels,
	'public' => true,
	'menu_icon' => 'dashicons-portfolio',
	'show_ui' => true,
	'capability_type' => 'post',
	'hierarchical' => true,
	'rewrite' => array( 'slug' => 'proyectos'),
	'query_var' => false,
	'has_archive' => true,
	'supports' => array('title', 'editor', 'author', 'thumbnail', 'revisions')
));


add_action( 'init', 'build_taxonomies', 0 );

function build_taxonomies() {
    register_taxonomy( 'tipo', 'proyectos', array( 'hierarchical' => true, 'label' => 'Tipo', 'show_in_nav_menus' => true, 'query_var' => true, 'rewrite' => array( 'slug' => 'tipo' ) ) ); 
}


/*
* Añade estilos personificados a la barra de herramientas de tiny_mce
*/
    // Callback function to insert 'styleselect' into the $buttons array
    function my_mce_buttons_2( $buttons ) {
    	array_unshift( $buttons, 'styleselect' );
    	return $buttons;
    }
    // Register our callback to the appropriate filter
    add_filter( 'mce_buttons_2', 'my_mce_buttons_2' );
    

    // Callback function to filter the MCE settings
    function my_mce_before_init_insert_formats( $init_array ) {  
    	// Define the style_formats array
    	$style_formats = array(  
    		// Each array child is a format with it's own settings 
    		array(  
    			'title' => '.entradilla',  
    			'block' => 'p',  
    			'classes' => 'entradilla',
    			'wrapper' => false,
    			
    		)
    	);  
    	// Insert the array, JSON ENCODED, into 'style_formats'
    	$init_array['style_formats'] = json_encode( $style_formats );  
    	
    	return $init_array;  
      
    } 
    // Attach callback to 'tiny_mce_before_init' 
    add_filter( 'tiny_mce_before_init', 'my_mce_before_init_insert_formats' );  
    
    add_editor_style( 'css/custom-editor-style.css' );





function ETG_content_banda_de_texto ($id) {
    $contenido = get_sub_field('titular', $id);
    return $contenido;
}


function ETG_content_galeria_de_videos ($id) {
    $contenido = get_sub_field('titular', $id);
    return $contenido;
}





function borrar_datos_fechas($post_id) {
    $args = array(
            'post_type' => 'agenda',
            'post_ID' => $post_id
    );
    $the_query = new WP_Query( $args );
    if ( $the_query->have_posts() ) {
    	while ( $the_query->have_posts()) {
    		$the_query->the_post();
    		if( have_rows('fechas') ){
                $enlace = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
                if ($enlace){
                    mysqli_query($enlace, "DELETE FROM etghaatik_fechas WHERE idEvento = ".$post_id);
                } else {
                    echo "Error al conectar a la base de datos.";
                }
    		}
    	}
    }
}


function guardar_datos_fechas($post_id) {
    /* 
        Esta función se ejecuta cada vez que un evento
        es guardado en el administardor del eventos de WP
    */
    $args = array(
            'post_type' => 'agenda',
            'p' => $post_id
    );
    $the_query = new WP_Query( $args );
    if ( $the_query->have_posts() ) {
    	while ( $the_query->have_posts() ) {
    		$the_query->the_post();
    		if( have_rows('fechas') ){
        		if (get_post_status($post_id) == 'publish'){
            		$enlace = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
                    if ($enlace){
                        mysqli_query($enlace, "DELETE FROM etghaatik_fechas WHERE idEvento = ".$post_id);
                		while ( have_rows('fechas') ) : the_row();
                    		$fecha = explode('/',get_sub_field('fecha'));
                    		$sql = "INSERT INTO etghaatik_fechas (idEvento, fecha, idioma) VALUES ('".$post_id."', '".$fecha[2]."-".$fecha[1]."-".$fecha[0]."', '".IDIOMA_AGENDA."')";
                    		$resultado = mysqli_query($enlace, $sql);
                    		if (!$resultado) {
                    			die("Error: ".mysqli_error());
                    		}
                        endwhile;
                    } else {
                        echo "Error al conectar a la base de datos.";
                    }
                }
    		}
    	}
    }
}

add_action( 'save_post', 'guardar_datos_fechas' );
add_action( 'before_delete_post', 'borrar_datos_fechas' );




function ETG_eventos($argsSnippet) {
    echo '<div class="col-md-6 evento_snippet">';
    echo '<div class="row">';
    echo '        <div class="col-md-9">';
    echo '            <h3 class="text-light text-uppercase"><a href="'.$argsSnippet['url'].'">'.$argsSnippet['titulo'].'</a></h3>';
    echo '            <p>';
    echo '                <span class="glyphicon glyphicon-time" aria-hidden="true"></span> '.$argsSnippet['fecha'];
    if ($argsSnippet['lugar']){
    echo '                    <br />';
    echo '                    <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span> '.$argsSnippet['lugar'];
    }
    echo '            </p>';
    if ($argsSnippet['link_entradas']){;
    echo '                <p><a href="'.$argsSnippet['link_entradas'].'" target="_blank" class="btn btn-primary"><span class="glyphicon glyphicon-tag" aria-hidden="true"></span>  '.__('Sarrerak erosi', 'ETG_text_domain').'</a></p>';
    }
    echo '        </div><!-- .col-md-8 -->';
    echo '</div><!-- row -->';
    echo '</div><!-- evento -->';
}







function ETG_activar_custom_post_type() {
        
    $labels = array(
    	'name'               => _x( 'Agenda', 'post type general name', 'ETG_kursaal' ),
    	'singular_name'      => _x( 'Evento', 'post type singular name', 'ETG_kursaal' ),
    	'menu_name'          => _x( 'Eventos', 'admin menu', 'ETG_kursaal' ),
    	'name_admin_bar'     => _x( 'Evento', 'add new on admin bar', 'ETG_kursaal' ),
    	'add_new'            => _x( 'Agregar nuevo', 'book', 'ETG_kursaal' ),
    	'add_new_item'       => __( 'Agregar nuevo evento', 'ETG_kursaal' ),
    	'new_item'           => __( 'Nuevo evento', 'ETG_kursaal' ),
    	'edit_item'          => __( 'Editar evento', 'ETG_kursaal' ),
    	'view_item'          => __( 'Ver evento', 'ETG_kursaal' ),
    	'all_items'          => __( 'Todos los eventos', 'ETG_kursaal' ),
    	'search_items'       => __( 'Buscar eventos', 'ETG_kursaal' ),
    	'parent_item_colon'  => __( 'Eventos hijo:', 'ETG_kursaal' ),
    	'not_found'          => __( 'No se encontraron eventos.', 'ETG_kursaal' ),
    	'not_found_in_trash' => __( 'No se encontraron eventos en la papelera.', 'ETG_kursaal' )
    );
    
    
    
    register_post_type('agenda', array(
    	'labels' => $labels,
    	'public' => true,
    	'menu_icon' => 'dashicons-calendar-alt',
    	'show_ui' => true,
    	'capability_type' => 'post',
    	'hierarchical' => false,
    	'rewrite' => array( 'slug' => 'agenda','with_front' => true),
    	'query_var' => true,
    	'has_archive' => true,
    	'supports' => array('title', 'editor', 'author', 'thumbnail', 'revisions')
    ));
    
    register_taxonomy( 'tipo', 'agenda', array( 'hierarchical' => true, 'label' => 'Tipo', 'show_in_nav_menus' => true, 'query_var' => true, 'rewrite' => array( 'hierarchical' => true ) ) ); 
}

add_action( 'init', 'ETG_activar_custom_post_type', 0 );


