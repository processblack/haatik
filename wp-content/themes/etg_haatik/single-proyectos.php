<?php get_header(); ?>

<article id="contenido" class="home">
        <?php 
        if ( have_posts() ) { 
            $i = 0;
            while ( have_posts() ) { 
                the_post();
            ?>
            <?php 
            if(get_field('foto_principal')){ 
                $foto = get_field('foto_principal'); 
                if (get_field('alineacion_foto')){
                    $alineacion_foto = get_field('alineacion_foto');
                } else {
                    $alineacion_foto = '50%';
                }
            ?>
            
                <div class="hero" style="background: #000 url(<?php echo $foto['url']; ?>) no-repeat <?php echo $alineacion_foto; ?>; background-size: cover; width: 100%; height: 500px;">
                <h2><?php the_title(); ?></h2>
                </div>
            <?php 
            }
            ?>
            <div class="container">
                <div class="row pd30_0">
                    <div class="col-md-7">
                        <h3 class="text-uppercase"><?php echo __('Sinopsia', 'ETG_text_domain'); ?></h3>
                        <?php the_content(); ?>
                    </div><!-- .col-md-8 -->
                    <div class="col-md-4 col-md-offset-1">
                    	<h3 class="text-uppercase"><?php echo __('Fitxa Artistikoa', 'ETG_text_domain'); ?></h3>
                    	<?php the_field('ficha_tecnica'); ?>
                    </div><!-- .col-md-4 -->
                </div><!-- .row -->
            </div><!-- .container -->
                            
            <?php
            if( have_rows('crear_contenido') ):
                while ( have_rows('crear_contenido') ) : the_row();
                    if( get_row_layout() == 'banda_de_foto' ):
                        get_template_part('contenidos/banda-de-foto');
                    
                    elseif( get_row_layout() == 'banda_de_texto' ): 
                        get_template_part('contenidos/banda-de-texto');
                    
                    elseif( get_row_layout() == 'galeria_de_fotos' ): 
                        get_template_part('contenidos/galeria-de-fotos');
                    
                    elseif( get_row_layout() == 'galeria_de_videos' ): 
                        get_template_part('contenidos/galeria-de-videos');
                    
                    endif;
                endwhile;
            endif;

            $i++;
            } 
        } wp_reset_postdata(); ?>
	</article>
	
<?php #get_sidebar(); ?>
<?php get_footer(); ?>