<?php 
    // Template Name: Página de home
    get_header();
?>
<article id="contenido" class="inicio">
	<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
		
    <?php if (get_field('slides')) { ?>
    	<div class="fondo-color">
            <div class="cycle-slideshow hero" 
                data-cycle-fx="scrollHorz" 
                data-cycle-timeout="4000"
                data-cycle-slides=".slide"
                data-cycle-log="false"
                data-cycle-pause-on-hover="true"
                data-cycle-pager="#bullets"
                data-cycle-pager-template="<div><span>{{slideNum}}</span></div>"
                data-cycle-prev="#prev"
                data-cycle-next="#next"
                >
                    
    			<?php 
        			$slides = 0;
        		foreach (get_field('slides') as $slide){
                ?>
    			<div class="slide parallax" style="" data-parallax="scroll" data-image-src="<?php echo $slide['fotografia']['sizes']['slide-grande']; ?>"">
                    <?php if ($slide['link']) { ?>
                    <a href="<?php echo $slide['link']; ?>" class="item">
                    <?php } else { ?>
                    <div class="item">
                    <?php } ?>
                        
                        <div class="caption">
                            <img src="<?php echo $slide['fotografia']['sizes']['slide-grande']; ?>" alt="<?php echo $slide['titular_fotografia']; ?>" class="img-responsive visible-xs" />
                            <?php if ($slide['titular'] !== "" or $slide['texto'] !== "") {
                                switch ($slide['texto_alinear']) {
                                    case 'izquierda': 
                                        $alinear = 'text-left';
                                    break;
                                    case 'derecha': 
                                        $alinear = 'text-right';
                                    break;
                                    default: 
                                        $alinear = 'text-center';
                                    break;
                                } 
                            ?>
                            <div class="container">
                            	<div class="row">
                            	    <div class="col-md-12">
                                	    <div class="texto" style="color: <?php echo ( $slide['color_texto'] ? $slide['color_texto'] : '#000');  ?> !important"><?php echo $slide['titular']; ?></div>
                            	    </div><!-- .col-md-8 -->
                            	</div><!-- .row -->
                            </div><!-- .container -->
                            <?php } ?>
                        </div>
                    <?php if ($slide['link']) { ?>
                    </a>
                    <?php } else { ?>
                    </div>
                    <?php } ?>
                </div>
        		<?php 
            		$slides++;
                }	
                if ($slides > 1) {
                ?>         
                <div class="navegador">
                    <a href="#" id="prev"><span>Prev</span></a> 
                    <a href="#" id="next"><span>Next</span></a>
                </div>
                <?php } ?>
            </div><!-- #slide_home -->      
        </div>	
    <?php } ?>
    
<!--     <div id="bullets" class="clearfix"></div> -->
    
    
    <div style="background-color: #cac1c3; padding: 40px 0 70px 0;" class="clearfix">
        <div class="container">
	    	<div class="row">
	    	    <div class="col-md-12">
	    	    	<h2 class="icono icono-eventos text-uppercase"><?php echo __('Hurrengo emanaldiak', 'ETG_text_domain'); ?></h2>
	    	    </div><!-- .col-md-12 -->
	    	</div><!-- .row -->
	    	<div class="row">
	    	<?php 
                $la_fecha = date('Y-m-d');
                $sqlAgenda = "SELECT * FROM (select * from etghaatik_fechas where (fecha >= '".$la_fecha."') and (idioma = '".IDIOMA_AGENDA."') order by fecha asc) as temp_table group by idEvento order by fecha asc limit 0, 2";

                global $wpdb;
                $resultados = $wpdb->get_results( $sqlAgenda, ARRAY_A );
                
                #echo count($resultados);
                
                set_query_var('resultados', $resultados); # pasa los valores
            	foreach ($resultados as $key=>$value) {
                    if (get_post_status($value['idEvento']) == 'publish'){ 
                        $idEvento = $value['idEvento'];
                        $fecha = get_field('fechas', $idEvento);
                        // Fechas 
                        setlocale(LC_TIME,MY_LOCALE);        
                        if (count($fecha) == 1) {
                            if (isset($fecha[0]['fecha'])){
                                $fecha_comprimida = str_replace('/', '-', $fecha[0]['fecha']);
                            }
                            $fecha_comprimida_dia = date("d", strtotime($fecha_comprimida));
                            $fecha_comprimida_mes = strftime("%b", strtotime("$fecha_comprimida"));
                            
                            $dia_evento = date("d", strtotime($fecha_comprimida));
                            $dia_texto_evento = strftime("%A", strtotime("$fecha_comprimida"));
                            $mes_evento = strftime("%B", strtotime("$fecha_comprimida"));
                            $ano_evento = date("Y", strtotime($fecha_comprimida));
                            if (isset($fecha[0]['hora'])){
                                $hora_evento = $fecha[0]['hora'];
                            }
                            
                            
                            $idioma = ICL_LANGUAGE_CODE;
                        	switch ($idioma){
                        		case "es":
                                    $fecha_texto = $dia_texto_evento.' <b>'.$dia_evento.'</b> de '.$mes_evento.' / <b>'.$hora_evento.'</b>'; # Martes 21 de octubre / 20:00
                        		break;
                        		case "eu":
                                    $fecha_texto = $mes_evento.'k <b>'.$dia_evento.'</b> '.$dia_texto_evento.' / <b>'.$hora_evento.'</b>(e)tan'; # Martes 21 de octubre / 20:00
                        		break;
                        		case "en":
                                    $fecha_texto = $dia_texto_evento.' <b>'.$dia_evento.'</b>th '.$mes_evento.' / <b>'.$hora_evento.'</b>'; # Martes 21 de octubre / 20:00
                        		break;
                        		case "fr":
                                    define('MY_LOCALE', 'fr_FR.UTF-8');
                                    $fecha_texto = $dia_texto_evento.' <b>'.$dia_evento.'</b> '.$mes_evento.' / <b>'.$hora_evento.'</b>'; # Martes 21 de octubre / 20:00
                        		break;
                        		default:
                                    $fecha_texto = $dia_texto_evento.' <b>'.$dia_evento.'</b> de '.$mes_evento.' / <b>'.$hora_evento.'</b>'; # Martes 21 de octubre / 20:00
                        		break;
                        	}
                            
                            
                            
                        } else {
                            #print_r($fecha);
                            $fecha_comprimida = str_replace('/', '-', $fecha[0]['fecha']);
                            $fecha_comprimida_dia = date("d", strtotime($fecha_comprimida));
                            $fecha_comprimida_mes = strftime("%b", strtotime("$fecha_comprimida"));
                            $fecha_texto = $fecha_texto;
                        }
    /*
                        echo $fecha_comprimida.'<br />';
                        echo $fecha_comprimida_dia.'<br />';;
                        echo $fecha_comprimida_mes.'<br />';;
                        echo $fecha_texto.'<br />';;
    */
                        
                        $argsSnippet = array(
                            'id'              => $idEvento,
                            'titulo'          => get_the_title($idEvento),
                			'fecha'           => $fecha_texto,
                			'lugar'           => get_field('lugar', $idEvento),
                			'url'             => get_permalink($idEvento),
                			'foto'            => get_the_post_thumbnail($idEvento, 'agenda-square', array('id' => 'imagen_'.$n, 'class' => 'img-max')),
                			'descripcion'     => get_field('descripcion_corta',$idEvento),
                			'link_entradas'   => get_field('link_entradas',$idEvento),
                			'tieneMarra'      => 'si',
                			'posicionEntrada' => 'arriba',
                			'transparencia'   => '',
                			'colorRandom'     => true
                        );
                        #print_r($argsSnippet);
                        
                        ETG_eventos($argsSnippet);
                        
            		} else {
                		continue;
            		}             			
                }
            ?>
            </div>
        </div>
    </div>
    
    
        
    
    
    <div class="container">
    	<div class="row">
    	    <div class="col-md-6">
    	    	<h2 class="icono icono-noticias text-uppercase"><?php echo __('Azken Albisteak', 'ETG_text_domain'); ?></h2>
            </div>
        </div>
        <div class="row">
                    <?php 
                    $i = 1;
                    // the query
                    $the_query = new WP_Query( array(
                    'posts_per_page' => 2
                    )); 
                    ?>
                    <?php if ( $the_query->have_posts() ) : ?>
                        <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                            <div class="col-md-6">
                                <div class="noticia">
                                <?php if(has_post_thumbnail()){ ?>
                                    <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'noticias', array('class' => 'img-responsive') ); ?></a>
                                <?php } ?>
                                <div class="margen">
                                    <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                    <?php the_excerpt(); ?>
                                    
                                    <p class="herramientas">
                                        <?php the_category( ', ' ); ?> · 
                                        <strong><?php echo __('Compartir', 'ETG_kursaal'); ?></strong>:
                                        <a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" target="_blank">Facebook</a> · 
                                        <a href="https://twitter.com/share?url=<?php the_permalink(); ?>&text=Nombre del producto" target="_blank">Twitter</a>
                                        <a href="https://plus.google.com/share?url=<?php the_permalink(); ?>" target="_blank">Google +</a>
                                        <a href="https://www.linkedin.com/shareArticle?url=<?php the_permalink(); ?>" target="_blank">LinkedIn</a>

                                    </p>
                                </div>
                                </div>
                            </div>
                            <?php $i++; ?>
                        <?php endwhile; ?>
                        <?php wp_reset_postdata(); ?>
                        <?php else : ?>
                            <p><?php __('No hay noticias', 'ETG_text_domain'); ?></p>
                    <?php endif; ?>    
    	    </div><!-- .col-md-6 -->
    	</div><!-- .row -->
    	
    	
    </div><!-- .container -->
    
    

<?php 
            if( have_rows('crear_contenido') ):
                while ( have_rows('crear_contenido') ) : the_row();
                    if( get_row_layout() == 'banda_de_foto' ):
                        get_template_part('contenidos/banda-de-foto');
                    
                    elseif( get_row_layout() == 'banda_de_texto' ): 
                        get_template_part('contenidos/banda-de-texto');
                    
                    elseif( get_row_layout() == 'galeria_de_fotos' ): 
                        get_template_part('contenidos/galeria-de-fotos');
                    
                    elseif( get_row_layout() == 'galeria_de_videos' ): 
                        get_template_part('contenidos/galeria-de-videos');
                    
                    elseif( get_row_layout() == 'galeria_de_videos' ): 
                        get_template_part('contenidos/galeria-de-videos');
                    
                    elseif( get_row_layout() == 'destacado' ): 
                        get_template_part('contenidos/destacado');
                    
                    endif;
                endwhile;
            endif;
    
?>
    
<!--
    <div id="destacados">
        <div class="container">
                
            <div class="row">
                <div class="col-sm-4">
                	<div class="snippet">
                    	<?php
                        $foto = get_field('fotografia_1');
                        ?>
                        <a href="<?php echo get_field('link_1'); ?>">
                            <img src="<?php echo $foto['sizes']['ficha']; ?>" alt="<?php echo get_field('titular_1'); ?>" class="img-responsive">
                            <span></span>
                            <h2><?php echo get_field('titular_1'); ?></h2>
                        </a>
                    </div>
                	<?php #echo get_field('texto_1'); ?>
                </div>
                <div class="col-sm-4">
                	<div class="snippet">
                    	<?php
                        $foto = get_field('fotografia_2');
                        ?>
                        <a href="<?php echo get_field('link_2'); ?>">
                            <img src="<?php echo $foto['sizes']['ficha']; ?>" alt="<?php echo get_field('titular_2'); ?>" class="img-responsive">
                            <span></span>
                            <h2><?php echo get_field('titular_2'); ?></h2>
                        </a>
                    </div>
                	<?php #echo get_field('texto_1'); ?>
                </div>
                <div class="col-sm-4">
                	<div class="snippet">
                    	<?php
                        $foto = get_field('fotografia_3');
                        ?>
                        <a href="<?php echo get_field('link_3'); ?>">
                            <img src="<?php echo $foto['sizes']['ficha']; ?>" alt="<?php echo get_field('titular_3'); ?>" class="img-responsive">
                            <span></span>
                            <h2><?php echo get_field('titular_3'); ?></h2>
                        </a>
                    </div>
                	<?php #echo get_field('texto_1'); ?>
                </div>
            </div>
        </div>
    </div>
-->

	<?php endwhile; ?>
</article>
<?php get_footer(); ?>