<div class="banda-de-texto" style="background-color: <?php the_sub_field('color_de_fondo'); ?>; color: <?php the_sub_field('color_de_texto'); ?> !important;">
	<div class="container">
		<div class="row">
		    <div class="col-md-<?php the_sub_field('numero_de_columnas'); ?> col-md-offset-<?php the_sub_field('offset'); ?>">
                <?php the_sub_field('texto'); ?>
		    </div><!-- .col-md-6 -->
		</div><!-- .row -->
	</div><!-- .container -->
</div>