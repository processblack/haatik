<?php 
    $foto        = get_sub_field('fotografia');
    $color_texto = get_sub_field('color_texto');
    if ($color_texto == ''){
        $color_texto = '#fff';
    }

    if (get_sub_field('alineacion_foto')){
        $alineacion_foto = get_sub_field('alineacion_foto');
    } else {
        $alineacion_foto = '50%';
    }
?>

<a href="<?php echo get_sub_field('link'); ?>" class="hero_cuarto parallax" style="background: <?php echo get_sub_field('color'); ?>; width: 100%; max-height: 500px; color: <?php echo $color_texto; ?> !important;" data-parallax="scroll" data-image-src="<?php echo $foto['url']; ?>">
    <div class="centrador">
    <div class="container">
    	<div class="row">
    	    <div class="col-md-12">
    	    	<?php if (get_sub_field('texto_de_apoyo')) { ?><p class="apoyo text-center text-uppercase"><?php echo get_sub_field('texto_de_apoyo'); ?></p><?php } ?>
    	    	<h3 class="titulo text-center" style="color: <?php echo $color_texto; ?>; font-weight: normal"><?php echo get_sub_field('titular'); ?></h3>
    	    </div><!-- .col-md-12 -->
    	</div><!-- .row -->
    	<div class="row">
    	    <div class="col-md-6 col-md-offset-3" style="color: <?php echo $color_texto; ?> !important;">
    	    	<?php echo get_sub_field('texto_largo'); ?>
    	    </div><!-- .col-md-6 -->
    	</div><!-- .row -->
    </div><!-- .container -->
    </div>
</a>

