<?php
    if( have_rows('videos') ) {
    while ( have_rows('videos') ) : the_row();
        if (get_sub_field('texto')) {
            $class_video = "col-md-8";
        } else {
            $class_video = "col-md-8 col-md-offset-2";            
        }
        ?>
        <div class="galeria-de-video" style="background: black; padding: 50px 0;">
            <div class="container">            	
                <div class="row">
                    <?php if (get_sub_field('texto')) { ?>
                    <div class="col-md-4">
                        <h3 class="text-uppercase blanco"><?php the_sub_field('titulo'); ?></h3>
                        <div class="texto-blanco">
                            <?php the_sub_field('texto'); ?>
                        </div>
                    </div><!-- .col-md-4 -->
                    <?php } ?>
                    <div class="<?php echo $class_video; ?>">
                        <div class="embed-container">
                            <?php 
                                $iframe = get_sub_field('video'); 
                                // use preg_match to find iframe src
                                preg_match('/src="(.+?)"/', $iframe, $matches);
                                $src = $matches[1];
                                
                                
                                // add extra params to iframe src
                                $params = array(
                                    'title'    => 0,
                                    'byline'        => 0,
                                    'portrait'    => 0
                                );
                                
                                $new_src = add_query_arg($params, $src);
                                
                                $iframe = str_replace($src, $new_src, $iframe);
                                
                                
                                // add extra attributes to iframe html
                                $attributes = 'frameborder="0"';
                                
                                $iframe = str_replace('></iframe>', ' ' . $attributes . '></iframe>', $iframe);
                                
                                
                                // echo $iframe
                                echo $iframe;
                                ?>
                        </div>
                    </div><!-- .col-md-12 -->
                </div><!-- .row -->
            </div><!-- .container -->
        </div>
        
        <?php
    endwhile;
    }
?>	    


