<?php 
    $foto        = get_sub_field('fotografia');
    $color_texto = get_sub_field('color_texto');
    if ($color_texto == ''){
        $color_texto = '#fff';
    }

    if (get_sub_field('alineacion_foto')){
        $alineacion_foto = get_sub_field('alineacion_foto');
    } else {
        $alineacion_foto = '50%';
    }
?>

<!-- <div class="hero_medio" style="background: <?php echo get_sub_field('color'); ?> url(<?php echo $foto['url']; ?>) no-repeat <?php echo $alineacion_foto ?>; background-size: cover; width: 100%; height: 500px;"> -->
<div class="hero_medio" style="background-size: cover; width: 100%; height: 500px;"  data-parallax="scroll" data-image-src="<?php echo $foto['url']; ?>">
    <div class="centrador">
    <div class="container">
    	<div class="row">
    	    <div class="col-md-12">
    	    	<h3 class="titulo" style="color: <?php echo $color_texto; ?>"><?php echo get_sub_field('texto'); ?></h3>
    	    </div><!-- .col-md-12 -->
    	</div><!-- .row -->
    	<div class="row">
    	    <div class="col-md-6 col-md-offset-3" style="color: <?php echo $color_texto; ?> !important;">
    	    	<?php echo get_sub_field('texto_largo'); ?>
    	    </div><!-- .col-md-6 -->
    	</div><!-- .row -->
    </div><!-- .container -->
    </div>
</div>
