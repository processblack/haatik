<?php get_header(); ?>


<article id="contenido" class="single">
    <div class="container">
        <?php 
        if ( have_posts() ) { 
            $i = 0;
            while ( have_posts() ) { 
                the_post();
            ?>
                
                <?php 
                if(has_post_thumbnail($the_query->ID)){ ?>
                    <div class="top">
                        <div class="row">
                            <div class="col-md-12">
                                <?php the_post_thumbnail('slide', array('class' => 'img-responsive force-max')); ?>
                            </div><!-- .col-md-12 -->
                        </div><!-- .row -->
                    </div>
                <?php 
                }
                ?>
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                    </div><!-- .col-md-12 -->
                </div><!-- .row -->
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="post">
                    		<div class="info">
                                <p class="fecha"><?php echo get_the_date('d\/m\/Y'); ?></p>
                                <p class="martel"><?php echo the_content(); ?></p>

                                <p class="herramientas">
                                    <?php the_category( ', ' ); ?> · 
                                    <strong><?php echo __('Compartir', 'ETG_kursaal'); ?></strong>:
                                    <a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" target="_blank">Facebook</a> · 
                                    <a href="https://twitter.com/share?url=<?php the_permalink(); ?>&text=Nombre del producto" target="_blank">Twitter</a>
                                    <a href="https://plus.google.com/share?url=<?php the_permalink(); ?>" target="_blank">Google +</a>
                                    <a href="https://www.linkedin.com/shareArticle?url=<?php the_permalink(); ?>" target="_blank">LinkedIn</a>
    
                                </p>
                            </div>
                        </div>
                    </div><!-- .col-md-8 -->
                    <!-- <div class="col-md-4 col-md-offset-1">
                    	<?php #get_sidebar(); ?>
                    </div>.col-md-4 -->
                </div><!-- .row -->
        </div>
        
        <?php  
                
                $i++;
            } 
        } wp_reset_postdata(); ?>
        
        
        
        <?php 
                
        if( have_rows('crear_contenido') ):
            while ( have_rows('crear_contenido') ) : the_row();
                if( get_row_layout() == 'banda_de_foto' ):
                    get_template_part('contenidos/banda-de-foto');
                
                elseif( get_row_layout() == 'banda_de_texto' ): 
                    get_template_part('contenidos/banda-de-texto');
                
                elseif( get_row_layout() == 'galeria_de_fotos' ): 
                    get_template_part('contenidos/galeria-de-fotos');
                
                elseif( get_row_layout() == 'galeria_de_videos' ): 
                    get_template_part('contenidos/galeria-de-videos');
                
                endif;
            endwhile;
        endif;
              
        ?>
        <div class="container">
        	<div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="comentarios">
                        <?php #comments_template(); ?>
                    </div>
                </div>
            </div><!-- .row -->
        </div><!-- .container -->        
        
</article>
	
<?php #get_sidebar(); ?>
<?php get_footer(); ?>
