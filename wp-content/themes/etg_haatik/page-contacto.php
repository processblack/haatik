<?php 
    // Template Name: Página de contacto
    #$GLOBALS['inicializarMapa'] = true;
    get_header();
?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
    <article id="contenido" class="contacto">
            <?php 
            if(has_post_thumbnail($the_query->ID)){ 
                
                if (get_field('alineacion_foto')){
                    $alineacion_foto = get_field('alineacion_foto');
                } else {
                    $alineacion_foto = '50%';
                }
            ?>
                <div class="hero_medio" style="background: #000 url(<?php the_post_thumbnail_url( 'full' ); ?>) no-repeat <?php echo $alineacion_foto; ?>; background-size: cover; width: 100%; height: 500px;">
                </div>
            <?php 
            }
            ?>
        <div class="container">            
            <div class="texto pd50_0">
            	<div class="row">
            	    <div class="col-md-12">
            	    	<h2 class="titular"><?php the_title();?></h2>
            	    </div><!-- .col-md-12 -->
            	</div><!-- .row -->
                <div class="row">
                    <div class="col-md-4">
                		<?php the_content();?>
                    </div><!-- .col-md-4 -->
                    <div class="col-md-6 col-md-offset-2">
                    	<form id="form1" name="form1" method="post" action="" class="form-horizontal">
                    		<p class="clearfix form-group">
                    			<label for="nombre"><?php echo __('Izena', 'ETG_text_domain'); ?></label>
                    			<input type="text" name="nombre" id="nombre" class="form-control" />
                    		</p>
                    		<p class="clearfix form-group">
                    			<label for="telefono"><?php echo __('Telefonoa', 'ETG_text_domain'); ?></label>
                    			<input type="text" name="telefono" id="telefono" class="form-control" />
                    		</p>
                    		<p class="clearfix form-group">
                    			<label for="mail"><?php echo __('Posta elektronikoa', 'ETG_text_domain'); ?></label>
                    			<input type="text" name="mail" id="mail" class="form-control" />
                    		</p>
                    		<p class="clearfix form-group">
                    			<label for="mensaje"><?php echo __('Mezua', 'ETG_text_domain'); ?></label>
                    			<textarea name="mensaje" id="mensaje" rows="10" cols="40" class="form-control"></textarea>
                    		</p>
                    		<p class="empujar form-group">
                    			<input type="submit" name="enviar" id="enviar_formulario" value="<?php echo __('Bidali', 'ETG_text_domain'); ?>" class="btn btn-primary" />
                    		</p>
                    		<div id="resultadoMensaje"></div>
                    	</form>                	
                    </div><!-- .col-md-7 -->                
                </div>
            </div><!-- .row -->
        </div><!-- .container -->
    </article>
<?php endwhile; ?>
<?php get_footer(); ?>
