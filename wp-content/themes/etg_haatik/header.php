<!doctype html>
<html>
	<head>
	  	<meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
			
		<title><?php wp_title('&laquo;', true, 'right'); ?> <?php bloginfo('name'); ?></title>
		
        <!-- Bootstrap -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,700|Open+Sans:400,700" rel="stylesheet">
        <link href="<?php bloginfo( 'template_url' ); ?>/css/bootstrap.min.css" rel="stylesheet">
		<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>"/>
		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
        <script src="<?php bloginfo('template_url');?>/js/jquery.min.js"></script>
        <script src="<?php bloginfo('template_url');?>/js/plugins.js"></script>

		<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

		<?php wp_head(); ?>

	<?php if ($GLOBALS['inicializarMapa']) { ?>
	    <script type="text/javascript" src="//maps.google.com/maps/api/js?sensor=true"></script>
	    </head>	
	    <body <?php body_class($color); ?> onload="initialize()">
	<?php } else { ?>
	    </head>	
	    <body <?php body_class($color); ?>>
	<?php } ?>	
        

        <header id="cabecera">
            <div class="margen">
                <div class="container-fluid">
                	<div class="row">
                	    <div class="col-md-2 col-xs-12">
                            <a id="logo" href="<?php bloginfo('url');?>">
                                <img src="<?php bloginfo( 'template_url' ); ?>/img/logo.png" srcset="<?php bloginfo( 'template_url' ); ?>/img/logo@2x.png 2x" alt="<?php bloginfo('title');?>" class="img-responsive center-block completo" />
                                <img src="<?php bloginfo( 'template_url' ); ?>/img/logo-corto.png" srcset="<?php bloginfo( 'template_url' ); ?>/img/logo-corto@2x.png 2x" alt="<?php bloginfo('title');?>" class="img-responsive center-block corto" />
                            </a>
                            <div class="control-menu visible-xs"></div>
                	    </div><!-- .col-md-2 -->
                	    <div class="col-md-9 col-sm-9">
                            <nav id="menu">
                                <?php wp_nav_menu( array('menu' => 'Principal', 'container' => false, 'menu_class' => 'text-lowercase text-right' )); ?>
                                <?php # wp_nav_menu( array( 'container_class' => 'menu-stamp', 'menu' => 'Principal' , 'walker' => new Thumbnail_Walker) );  ?>
                            </nav>
                	    </div><!-- .col-md-9 -->
                	    <div class="col-md-1 col-xs-6">
                	    	<ul class="social">
                    	    	<li><a href="https://instagram.com/haatikdantzakonpainia"><img src="<?php bloginfo( 'template_url' ); ?>/img/instagram.png" alt="twitter"></a></li>
                    	    	<li><a href="https://vimeo.com/haatikdantzakonpainia"><img src="<?php bloginfo( 'template_url' ); ?>/img/vimeo.png" alt="twitter"></a></li>
                    	    	<li><a href="https://twitter.com/haatik_dantza"><img src="<?php bloginfo( 'template_url' ); ?>/img/twitter.png" srcset="<?php bloginfo( 'template_url' ); ?>/img/twitter@2x.png 2x" alt="twitter"></a></li>
                    	    	<li><a href="https://www.facebook.com/HaatikDantzaKonpainia"><img src="<?php bloginfo( 'template_url' ); ?>/img/facebook.png" srcset="<?php bloginfo( 'template_url' ); ?>/img/facebook@2x.png 2x" alt="facebook"></a></li>
                    	    </ul>
                	    </div><!-- .col-md-1 -->
                	</div><!-- .row -->
                </div><!-- .container -->
            </div>
            <div id="idiomas">
                <div class="container-fluid">
                	<div class="row">
                	    <div class="col-md-12">
                	    	<ul>
                            <?php 
                            	$idiomas = icl_get_languages('skip_missing=0&orderby=KEY&order=DIR&link_empty_to=str');
                            	$nIdiomas = count($idiomas);
                            	$i = 1;
                                #print_r($idiomas);
                            	foreach ($idiomas as $idioma) {
                                	$idArticulo = $wp_query->get_queried_object_id();
                                	$idArticuloIdioma = icl_object_id($idArticulo, 'agenda', 0,$idioma['language_code']);
                                	if ($idArticuloIdioma == ""){
                                    	$idArticuloIdioma = icl_object_id($idArticulo, 'agenda', 0,'es');
                                	}
                                	#echo '<!-- ->'.$idArticulo.$idioma['language_code'].$idArticuloIdioma.'-->';
                                	$linkIdioma = get_permalink($idArticuloIdioma);
                                        if ($idioma['url'] == 'str'){
                                            $url = $linkIdioma;
                                        } else {
                                            $url = $idioma['url'];
                                            if ($idioma['code'] == 'es'){
                                                $url = str_replace('/mota/', '/tipo/', $url);
                                                $url = str_replace('/merkataritzak/', '/comercios/', $url);
                                            } else {
                                                $url = str_replace('/tipo/', '/mota/', $url);
                                                $url = str_replace('/comercios/', '/merkataritzak/', $url);
                                                
                                            }
                                            #if ($idioma['active'] !== 1){
                                            #}
                                        }
                                    	   ?>
                                           <li><a href="<?php echo $url; ?>" title="<?php echo $idioma['native_name']; ?>" class="text-uppercase<?php if ($idioma['active'] == 1) echo ' current'; ?>"><?php echo $idioma['language_code']; ?></a></li>
                            <?php 
                                    $i++;
                            	}
                            ?>
                    	    </ul>
                	    </div><!-- .col-md-12 -->
                	</div><!-- .row -->
                </div><!-- .container -->
            </div>
        </header>
        
        
        <?php 
        $the_query = new WP_Query( array( 'post_type' => 'proyectos', 'orderby' => 'menu_order', 'order'   => 'asc') );
        if ( $the_query->have_posts() ) {
            ?>
            <div id="lanak" class="clearfix">
            <?php
                while ( $the_query->have_posts() ) {
                    $the_query->the_post();
                    echo '<div class="lana">';
                        echo '<a href="'.get_the_permalink(get_the_ID()).'">';
                            if (has_post_thumbnail(get_the_ID())){
                                echo get_the_post_thumbnail(get_the_ID(), 'poster', array('class' => 'img-responsive'));
                            } else {
                                echo '<img src="http://eltipografico.com/_herramientas/dummyimage/420x620/999/fff&text=" alt="TEST" class="img-responsive" />';
                            }
                            #echo '<span class="titulo">'.get_the_title().'</span>';
                    echo '</a>';
                    echo '</div>';
                }
            /* Restore original Post Data */
            wp_reset_postdata();
            ?>
        </div>
        <?php
        }       
        ?>
