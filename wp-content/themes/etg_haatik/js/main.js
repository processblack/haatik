

var scroll_top = $(window).scrollTop();


var sticky_navigation = function(){

    var posicion = $(window).scrollTop();
    var altura = $(window).height() / 2;
    var dondeVa = posicion + $(window).height();
    //console.log(posicion + " - " + dondeVa + " - " + $(document).height() + " - " + $('#pie').height() );

    if ($(window).width() > 768) {
    if (posicion <= 130){
        $('#cabecera').css('background-color', 'rgba(255,255,255,'+posicion/100+')');
    } else {
        if (posicion > 80){
            $('#top').css('background-color', '#fff');
        }
    }
    if (posicion <= 130){
        $('#cabecera img.completo').css('opacity', (130-posicion)/100);        
    }
    }

};


$(window).scroll(function() {
    sticky_navigation();
    parallaxScroll();
});
$(window).resize(function() {
    ETG_ajustar();
});
$(".boxer").boxer();


function ETG_ajustar(){
    if ($(window).width() > 768) {
        $('.hero').css('height', ($(window).height()-($('#cabecera').height()+80))+'px');
        $('.hero_medio').css('min-height', ($(window).height()/2)+'px');
        $('.hero_cuarto').css('min-height', ($(window).height()/3)+'px');
    }
}
ETG_ajustar();

if ($(window).width() > 768){
    $('#menu-item-148, #menu-item-3556, #menu-item-3898, #menu-item-3722').click(function(e){
        e.preventDefault();
        if ($(this).hasClass( 'active' )){
            $(this).removeClass( 'active' );
            $('#lanak').fadeOut();
        } else {
            $(this).addClass( 'active' );
            $('#lanak').fadeIn();
        }
    });
    
    $('#lanak').mouseleave(function(){
        $(this).fadeOut();
        $('#menu-item-17').removeClass( 'active' );
    });
}

$('.control-menu').click(function(e){
    $('#menu').toggleClass('on');
    e.preventDefault();
});

function parallaxScroll(){
    var scrolledY = $(window).scrollTop();
    console.log(scrolledY);
    $('.parallax').css('top','-'+((scrolledY*0.3))+'px');
}


/* 
    - - - - - - - - - - - - - - - - - - - - - - - - 
    Envía formularios
    - - - - - - - - - - - - - - - - - - - - - - - - 
*/
    $('#enviar_formulario').click(function(event) {
        $('#resultadoMensaje').empty();
        $('#resultadoMensaje').append('<p class="gris">Procesando mensaje</p>');
        
        var form_data = {
        // get the form values
            viaje   : $('#viaje').val(),
            nombre  : $('#nombre').val(),
            email   : $('#email').val(),
            telefono: $('#telefono').val(),
            mensaje : $('#mensaje').val(),
            empresa : $('#empresa').val()
        };

        // send the form data to the controller
        $.ajax({
            url: "/enviar-formulario-contacto.php",
            type: "post",
            data: form_data,
            dataType: "json",
            cache: false,
            success: function(msg)
            {
                if(msg.validate) {
                   //console.log("ok");
                   $('#resultadoMensaje').fadeIn();
                   $('#resultadoMensaje').empty();
                   $('#resultadoMensaje').append('<div class="gris"><p>Mensaje enviado!<br />En breve nos pondremos en contacto con usted.</p><p>Un saludo y gracias.</p>');
                   $('form#contacto').fadeOut();
                   $('#enviar_formulario').attr('disabled', 'disabled');
                   //$('#myModal').modal('hide');
                } else {
                   //console.log(msg.mensaje);
                   $('#resultadoMensaje').append('<p class="gris">Error al enviar el mensaje!<br />'+ msg.mensaje +'</p>');
                   $('#resultadoMensaje').fadeIn();
                }
            }
        });
        event.preventDefault();
        // prevents from refreshing the page
    });    
