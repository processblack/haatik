<?php get_header(); ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>


    <article id="contenido" class="pagina">
            <?php 
            if(has_post_thumbnail($the_query->ID)){ 
                
                if (get_field('alineacion_foto')){
                    $alineacion_foto = get_field('alineacion_foto');
                } else {
                    $alineacion_foto = '50%';
                }
            ?>
<!--                 <div class="hero_medio" style="background: #000 url(<?php the_post_thumbnail_url( 'full' ); ?>) no-repeat <?php echo $alineacion_foto; ?>; background-size: cover; width: 100%; height: 500px;"> -->
                <div class="hero_medio" style="background-size: cover; width: 100%; height: 500px;" data-parallax="scroll" data-image-src="<?php the_post_thumbnail_url( 'full' ); ?>">
                </div>
            <?php 
            }
            ?>

        <div class="container">
        	<div class="row">
        	    <div class="col-md-8 col-md-offset-2">
            		<div class="texto pd50_0">
                        <?php $ocultar_titular = get_field('ocultar_titular'); if ($ocultar_titular[0] !== 'si') { ?>
                        <h2 class="titular"><?php the_title();?></h2>
                        <?php } ?>
            		    <?php the_content();?>
            		</div><!-- .texto -->
        	    </div><!-- .col-md-8 -->
        	</div><!-- .row -->
        </div><!-- .container -->
        
        <?php
            if( have_rows('crear_contenido') ):
                while ( have_rows('crear_contenido') ) : the_row();
                    if( get_row_layout() == 'banda_de_foto' ):
                        get_template_part('contenidos/banda-de-foto');
                    
                    elseif( get_row_layout() == 'banda_de_texto' ): 
                        get_template_part('contenidos/banda-de-texto');
                    
                    elseif( get_row_layout() == 'galeria_de_fotos' ): 
                        get_template_part('contenidos/galeria-de-fotos');
                    
                    elseif( get_row_layout() == 'galeria_de_videos' ): 
                        get_template_part('contenidos/galeria-de-videos');
                    
                    endif;
                endwhile;
            endif;
            
            
            if( have_rows('miembros') ):

        		$equipo = get_field('miembros');
        		$equipo_dantzaris = array();
        		$equipo_otros = array();
        		foreach($equipo as $persona){
            		switch($persona['grupo']){
                		case "teknikoa":
                		    $equipo_otros[] = $persona;
                		break;
                		default:
                		    $equipo_dantzaris[] = $persona;
                		break;
            		}
        		}
        		#print_r($equipo_profesoras);
        		
        		
        		function pintar_equipo($datos){
            		$i = 0;
            		$n = 1;
            		foreach ($datos as $persona){
            	        $output .= '<div class="col-md-3 col-sm-4 col-xs-6">';
                        $output .= '<p><img src="'.$persona['fotografia']['sizes']['miniatura-grande'].'" alt="'.$persona['nombre'].'" class="img-responsive center-block" /></p>';
                        $output .= '<p class="text-center">';
                        $output .= '<strong>'.$persona['nombre'].'</strong><br />';
                        $output .= $persona['fecha_de_nacimiento'];
                        if ($persona['lugar_de_nacimiento']) { 
                            $output .= '('.$persona['lugar_de_nacimiento'].')';
                        } else { 
                            $output .= " &nbsp;"; 
                        } 
                        
                        $output .= '</p>';
                        $output .= '<!--<p>'.$persona['texto'].'</p> -->';
                        $output .= '</div><!-- .col-md-3 -->';
                        $n++;
                    }
            		return $output;
        		}
            ?>



            <div class="container pd50_0">
            	<div class="row">
                    <div class="col-md-12">
                    	<h3 class="text-uppercase"><?php echo __('Talde artistikoa', 'ETG_text_domain'); ?></h3>
                    </div><!-- .col-md-2 -->
            	</div><!-- .row -->
            	<div class="row">
                    <?php echo pintar_equipo($equipo_dantzaris); ?>
            	</div><!-- .row -->
            </div><!-- .container -->
            <div class="container pd50_0">
            	<div class="row">
                    <div class="col-md-12">
                    	<h3 class="text-uppercase"><?php echo __('Teknikariak', 'ETG_text_domain'); ?></h3>
                    </div><!-- .col-md-2 -->
            	</div><!-- .row -->
            	<div class="row">
                    <?php echo pintar_equipo($equipo_otros); ?>
            	</div><!-- .row -->
            </div><!-- .container -->
            <?php
            endif;            
            
        ?>
    </article>
<?php endwhile; ?>
<?php get_footer(); ?>


