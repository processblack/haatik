<?php get_header(); ?>


<article id="contenido" class="single">
        <?php 
        if ( have_posts() ) { 
            $i = 0;
            while ( have_posts() ) { 
                the_post();
            ?>
            <?php 
            if(has_post_thumbnail($the_query->ID)){ 
                if (get_field('alineacion_foto')){
                    $alineacion_foto = get_field('alineacion_foto');
                } else {
                    $alineacion_foto = '50%';
                }
                
            ?>
                <div class="hero_corto" style="background: #000 url(<?php the_post_thumbnail_url( 'full' ); ?>) no-repeat <?php echo $alineacion_foto; ?>; background-size: cover; width: 100%; height: 500px;">
                </div>
            <?php 
            }
            $fecha = get_field('fechas');
            setlocale(LC_TIME,MY_LOCALE);        
            if (count($fecha) == 1) {
                if (isset($fecha[0]['fecha'])){
                    $fecha_comprimida = str_replace('/', '-', $fecha[0]['fecha']);
                }
                $fecha_comprimida_dia = date("d", strtotime($fecha_comprimida));
                $fecha_comprimida_mes = strftime("%b", strtotime("$fecha_comprimida"));
                
                $dia_evento = date("d", strtotime($fecha_comprimida));
                $dia_texto_evento = strftime("%A", strtotime("$fecha_comprimida"));
                $mes_evento = strftime("%B", strtotime("$fecha_comprimida"));
                $ano_evento = date("Y", strtotime($fecha_comprimida));
                if (isset($fecha[0]['hora'])){
                    $hora_evento = $fecha[0]['hora'];
                }
                
                
                $idioma = ICL_LANGUAGE_CODE;
            	switch ($idioma){
            		case "es":
                        $fecha_texto = $dia_texto_evento.' <b>'.$dia_evento.'</b> de '.$mes_evento.' / <b>'.$hora_evento.'</b>'; # Martes 21 de octubre / 20:00
            		break;
            		case "eu":
                        $fecha_texto = $mes_evento.'k <b>'.$dia_evento.'</b> '.$dia_texto_evento.' / <b>'.$hora_evento.'</b>(e)tan'; # Martes 21 de octubre / 20:00
            		break;
            		case "en":
                        $fecha_texto = $dia_texto_evento.' <b>'.$dia_evento.'</b>th '.$mes_evento.' / <b>'.$hora_evento.'</b>'; # Martes 21 de octubre / 20:00
            		break;
            		case "fr":
                        define('MY_LOCALE', 'fr_FR.UTF-8');
                        $fecha_texto = $dia_texto_evento.' <b>'.$dia_evento.'</b> '.$mes_evento.' / <b>'.$hora_evento.'</b>'; # Martes 21 de octubre / 20:00
            		break;
            		default:
                        $fecha_texto = $dia_texto_evento.' <b>'.$dia_evento.'</b> de '.$mes_evento.' / <b>'.$hora_evento.'</b>'; # Martes 21 de octubre / 20:00
            		break;
            	}
                
                
                
            } else {
                #print_r($fecha);
                $fecha_comprimida = str_replace('/', '-', $fecha[0]['fecha']);
                $fecha_comprimida_dia = date("d", strtotime($fecha_comprimida));
                $fecha_comprimida_mes = strftime("%b", strtotime("$fecha_comprimida"));
                $fecha_texto = $fecha_texto;
            }
    /*
            echo $fecha_comprimida.'<br />';
            echo $fecha_comprimida_dia.'<br />';;
            echo $fecha_comprimida_mes.'<br />';;
            echo $fecha_texto.'<br />';;
    */
                        
            
            ?>
            <div class="post pd50_0">                        
    <div class="container">
    	<div class="row">
    	    <div class="col-md-12">
    	    	<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
    	    </div><!-- .col-md-12 -->
    	</div><!-- .row -->
    	<div class="row">
                <div class="col-md-7">
            		<div class="info">
                        
                        <p class="martel"><?php echo the_content(); ?></p>
                        <p><a href="<?php echo the_permalink(); ?>"></a></p>
                    </div>
                </div><!-- .col-md-8 -->
                <div class="col-md-4 col-md-offset-1">
                    <p>
                        <span class="glyphicon glyphicon-time" aria-hidden="true"></span> <?php echo $fecha_texto; ?><br />
                    </p>
                    <p>
                        <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span> <?php echo get_field('lugar'); ?>
                        <br />
                        <?php echo get_field('lugar_direccion'); ?>
                    </p>
                    
                    <?php if (get_field('link_entradas', $idEvento)){ ?>
                        <p class="sarrerak"><a href="<?php echo get_field('link_entradas', $idEvento) ?>" target="_blank" class="btn btn-lg btn-primary"><span class="glyphicon glyphicon-tag" aria-hidden="true"></span>  <?php echo __('Sarrerak erosi', 'ETG_text_domain'); ?></a></p>
                    <?php } ?>
                </div><!-- .col-md-4 -->
    	</div><!-- .row -->
    </div><!-- .container -->
            </div>
        <?php 
                
        if( have_rows('crear_contenido') ):
            while ( have_rows('crear_contenido') ) : the_row();
                if( get_row_layout() == 'banda_de_foto' ):
                    get_template_part('contenidos/banda-de-foto');
                
                elseif( get_row_layout() == 'banda_de_texto' ): 
                    get_template_part('contenidos/banda-de-texto');
                
                elseif( get_row_layout() == 'galeria_de_fotos' ): 
                    get_template_part('contenidos/galeria-de-fotos');
                
                elseif( get_row_layout() == 'galeria_de_videos' ): 
                    get_template_part('contenidos/galeria-de-videos');
                
                endif;
            endwhile;
        endif;
              
        ?>
        
        <?php  
                
                $i++;
            } 
        } wp_reset_postdata(); ?>
</article>
	
<?php #get_sidebar(); ?>
<?php get_footer(); ?>
