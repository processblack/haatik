#, fuzzy
msgid ""
msgstr ""
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"Project-Id-Version: Haatik\n"
"POT-Creation-Date: 2018-03-23 13:26+0100\n"
"PO-Revision-Date: 2018-03-23 13:26+0100\n"
"Last-Translator: \n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.0.6\n"
"X-Poedit-Basepath: ..\n"
"X-Poedit-Flags-xgettext: --add-comments=translators:\n"
"X-Poedit-WPHeader: style.css\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;"
"esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;"
"_nx_noop:3c,1,2;__ngettext_noop:1,2\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: *.js\n"

#: archive-agenda.php:23
msgid "Emanaldiak"
msgstr ""

#: archive-agenda.php:120 functions.php:299 single-agenda.php:106
msgid "Sarrerak erosi"
msgstr ""

#: archive.php:32 search.php:33
msgid "(more…)"
msgstr ""

#: archive.php:33 search.php:34
msgid "Leer más..."
msgstr ""

#: functions.php:129
msgctxt "post type general name"
msgid "Proyectos"
msgstr ""

#: functions.php:130
msgctxt "post type singular name"
msgid "Proyecto"
msgstr ""

#: functions.php:131
msgctxt "admin menu"
msgid "Proyectos"
msgstr ""

#: functions.php:132
msgctxt "add new on admin bar"
msgid "Proyecto"
msgstr ""

#: functions.php:133 functions.php:319
msgctxt "book"
msgid "Agregar nuevo"
msgstr ""

#: functions.php:134
msgid "Agregar nuevo proyecto"
msgstr ""

#: functions.php:135
msgid "Nuevo proyecto"
msgstr ""

#: functions.php:136
msgid "Editar proyecto"
msgstr ""

#: functions.php:137
msgid "Ver proyecto"
msgstr ""

#: functions.php:138
msgid "Todos los proyectos"
msgstr ""

#: functions.php:139
msgid "Buscar proyectos"
msgstr ""

#: functions.php:140
msgid "Proyectos hijo:"
msgstr ""

#: functions.php:141
msgid "No se encontraron proyectos."
msgstr ""

#: functions.php:142
msgid "No se encontraron proyectos en la papelera."
msgstr ""

#: functions.php:315
msgctxt "post type general name"
msgid "Agenda"
msgstr ""

#: functions.php:316
msgctxt "post type singular name"
msgid "Evento"
msgstr ""

#: functions.php:317
msgctxt "admin menu"
msgid "Eventos"
msgstr ""

#: functions.php:318
msgctxt "add new on admin bar"
msgid "Evento"
msgstr ""

#: functions.php:320
msgid "Agregar nuevo evento"
msgstr ""

#: functions.php:321
msgid "Nuevo evento"
msgstr ""

#: functions.php:322
msgid "Editar evento"
msgstr ""

#: functions.php:323
msgid "Ver evento"
msgstr ""

#: functions.php:324
msgid "Todos los eventos"
msgstr ""

#: functions.php:325
msgid "Buscar eventos"
msgstr ""

#: functions.php:326
msgid "Eventos hijo:"
msgstr ""

#: functions.php:327
msgid "No se encontraron eventos."
msgstr ""

#: functions.php:328
msgid "No se encontraron eventos en la papelera."
msgstr ""

#: index.php:26
msgid "Irakurtzen jarraitu..."
msgstr ""

#: index.php:49
msgid "Aurreko artikuluak"
msgstr ""

#: index.php:49
msgid "Hurrengo artikuluak"
msgstr ""

#: page-contacto.php:36
msgid "Izena"
msgstr ""

#: page-contacto.php:40
msgid "Telefonoa"
msgstr ""

#: page-contacto.php:44
msgid "Posta elektronikoa"
msgstr ""

#: page-contacto.php:48
msgid "Mezua"
msgstr ""

#: page-contacto.php:52
msgid "Bidali"
msgstr ""

#: page-home.php:83
msgid "Hurrengo emanaldiak"
msgstr ""

#: page-home.php:189
msgid "Azken Albisteak"
msgstr ""

#: page-home.php:213 single.php:39
msgid "Compartir"
msgstr ""

#: page-home.php:227
msgid "No hay noticias"
msgstr ""

#: page.php:58
msgid "Dantzariak"
msgstr ""

#: search.php:7
msgid "Resultados de la búsqueda"
msgstr ""

#: search.php:40
msgid "No se han encontrado resultados."
msgstr ""

#: single-proyectos.php:29
msgid "Sinopsia"
msgstr ""

#: single-proyectos.php:33
msgid "Fitxa Artistikoa"
msgstr ""

#. Theme Name of the plugin/theme
msgid "Haatik"
msgstr ""

#. Theme URI of the plugin/theme
msgid "http://www.haatik.com"
msgstr ""

#. Description of the plugin/theme
msgid "Theme del sitio"
msgstr ""

#. Author of the plugin/theme
msgid "El Tipo Gráfico."
msgstr ""

#. Author URI of the plugin/theme
msgid "http://www.eltipografico.com"
msgstr ""
