<?php get_header(); ?>

<article id="contenido">
        <?php 
        if ( have_posts() ) { 
            $i = 0;
            while ( have_posts() ) { 
                the_post();

                    ?>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6">
                                    <?php 
                                    if(has_post_thumbnail($the_query->ID)){ ?>
                                        <a href="<?php echo the_permalink(); ?>"><?php the_post_thumbnail('ficha', array('class' => 'img-responsive')); ?></a>
                                    <?php 
                                    }
                                    ?>
                            </div>
                            <div class="col-md-6">
                                    <p class="fecha"><?php echo get_the_date('d\/m\/Y'); ?></p>
                                    
                            		<div class="info">
                                        <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                        <?php the_excerpt(); ?><a href="<?php echo the_permalink(); ?>"><?php echo __('Irakurtzen jarraitu...', 'ETG_text_domain'); ?></a>
                                    </div>
                            	
                            </div><!-- .col-md-6 -->
                        </div><!-- row -->
                        <div class="row">
                            <div class="col-md-12">
                            	<hr />
                            </div><!-- .col-md-12 -->
                        </div><!-- .row -->
                    </div><!-- .container -->
                    <?php
                
            ?>
            <?php 
                $i++;
            }
            ?>
            
            
            <div class="container pd50_0">
            	<div class="row">
            	    <div class="col-md-12 text-center">
                        <?php posts_nav_link( ' &#183; ', '&laquo; '.__('Aurreko artikuluak', 'ETG_text_domain'), __('Hurrengo artikuluak', 'ETG_text_domain').' &raquo;' ); ?>
            	    </div><!-- .col-md-12 -->
            	</div><!-- .row -->
            </div><!-- .container -->
        <?php } wp_reset_postdata(); ?>
	</article>
	
<?php #get_sidebar(); ?>
<?php get_footer(); ?>