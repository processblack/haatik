<?php get_header(); ?>

<?php 
	
$la_fecha = date('Y-m-d');
#$sqlAgenda = "select * from etghaatik_fechas where (fecha >= '".$la_fecha."') and (idioma = '".IDIOMA_AGENDA."') group by idEvento order by fecha asc";

$sqlAgenda = "SELECT * FROM (select * from etghaatik_fechas where (fecha >= '".$la_fecha."') and (idioma = '".IDIOMA_AGENDA."') order by fecha asc) as temp_table group by idEvento order by fecha asc;";

#echo $sqlAgenda;

global $wpdb;
$resultados = $wpdb->get_results( $sqlAgenda, ARRAY_A );    		

set_query_var('resultados', $resultados); # pasa los valores
?>


<article id="contenido">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12">
            	<h2 class="text-uppercase"><?php echo __('Emanaldiak', 'ETG_text_domain'); ?></h2>
            </div><!-- .col-md-12 -->
        </div><!-- .row -->
        
        <?php 
        	
        	echo "<!--";
        	print_r($resultados);
        	echo "-->";
        	
        	foreach ($resultados as $key=>$value) {
                if (get_post_status($value['idEvento']) == 'publish'){ 

                    $idEvento = $value['idEvento'];
                    $fecha = get_field('fechas', $idEvento);
                    // Fechas 
                    setlocale(LC_TIME,MY_LOCALE);        
                    if (count($fecha) == 1) {
                        if (isset($fecha[0]['fecha'])){
                            $fecha_comprimida = str_replace('/', '-', $fecha[0]['fecha']);
                        }
                        $fecha_comprimida_dia = date("d", strtotime($fecha_comprimida));
                        $fecha_comprimida_mes = strftime("%b", strtotime("$fecha_comprimida"));
                        
                        $dia_evento = date("d", strtotime($fecha_comprimida));
                        $dia_texto_evento = strftime("%A", strtotime("$fecha_comprimida"));
                        $mes_evento = strftime("%B", strtotime("$fecha_comprimida"));
                        $ano_evento = date("Y", strtotime($fecha_comprimida));
                        if (isset($fecha[0]['hora'])){
                            $hora_evento = $fecha[0]['hora'];
                        }
                        
                        
                        $idioma = ICL_LANGUAGE_CODE;
                    	switch ($idioma){
                    		case "es":
                                $fecha_texto = $dia_texto_evento.' <b>'.$dia_evento.'</b> de '.$mes_evento.' / <b>'.$hora_evento.'</b>'; # Martes 21 de octubre / 20:00
                    		break;
                    		case "eu":
                                $fecha_texto = $mes_evento.'k <b>'.$dia_evento.'</b> '.$dia_texto_evento.' / <b>'.$hora_evento.'</b>(e)tan'; # Martes 21 de octubre / 20:00
                    		break;
                    		case "en":
                                $fecha_texto = $dia_texto_evento.' <b>'.$dia_evento.'</b>th '.$mes_evento.' / <b>'.$hora_evento.'</b>'; # Martes 21 de octubre / 20:00
                    		break;
                    		case "fr":
                                define('MY_LOCALE', 'fr_FR.UTF-8');
                                $fecha_texto = $dia_texto_evento.' <b>'.$dia_evento.'</b> '.$mes_evento.' / <b>'.$hora_evento.'</b>'; # Martes 21 de octubre / 20:00
                    		break;
                    		default:
                                $fecha_texto = $dia_texto_evento.' <b>'.$dia_evento.'</b> de '.$mes_evento.' / <b>'.$hora_evento.'</b>'; # Martes 21 de octubre / 20:00
                    		break;
                    	}
                        
                        
                        
                    } else {
                        #print_r($fecha);
                        $fecha_comprimida = str_replace('/', '-', $fecha[0]['fecha']);
                        $fecha_comprimida_dia = date("d", strtotime($fecha_comprimida));
                        $fecha_comprimida_mes = strftime("%b", strtotime("$fecha_comprimida"));
                        $fecha_texto = $fecha_texto;
                    }
/*
                    echo $fecha_comprimida.'<br />';
                    echo $fecha_comprimida_dia.'<br />';;
                    echo $fecha_comprimida_mes.'<br />';;
                    echo $fecha_texto.'<br />';;
*/
                    
                    
                    
                    
                    ?>
                    <div class="row">
                        <div class="col-md-6">
                                <?php 
                                if(has_post_thumbnail($idEvento)){ ?>
                                    <a href="<?php echo get_permalink($idEvento); ?>"><?php echo get_the_post_thumbnail($idEvento, 'ficha', array('class' => 'img-responsive')); ?></a>
                                <?php 
                                }
                                ?>
                            
                        </div>
                        <div class="col-md-6">
                        	
                                <div class="row">
                                    <div class="col-md-8">
                                		<div class="info">
                                            <h3><a href="<?php echo get_permalink($idEvento); ?>"><?php echo get_the_title($idEvento); ?></a></h3>
                                            <p>
                                                <span class="glyphicon glyphicon-time" aria-hidden="true"></span> <?php echo $fecha_texto; ?><br />
                                                <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span> <?php echo get_field('lugar', $idEvento); ?>
                                            </p>
                                        </div>
                                    </div><!-- .col-md-8 -->
                                    <div class="col-md-4">
                                        <?php if (get_field('link_entradas', $idEvento)){ ?>
                                            <p style="padding-top: 20px;" class="text-right"><a href="<?php echo get_field('link_entradas', $idEvento) ?>" target="_blank" class="btn btn-primary"><span class="glyphicon glyphicon-tag" aria-hidden="true"></span>  <?php echo __('Sarrerak erosi', 'ETG_text_domain'); ?></a></p>
                                        <?php } ?>                                
                                    </div><!-- .col-md-4 -->
                                </div><!-- .row -->
                        	
                        </div><!-- .col-md-6 -->

                    </div><!-- row -->
                    <div class="row">
                        <div class="col-md-12">
                        	<hr />
                        </div><!-- .col-md-12 -->
                    </div><!-- .row -->
                    <?php
                    
/*
                    $argsSnippet = array(
                        'id'              => $idEvento,
                        'categorias'      => $tipos,
                        'titulo'          => $titulo,
            			'fecha'           => get_field('fechas', $idEvento),
            			'fecha_texto'     => get_field('fecha_texto', $idEvento),
            			'url'             => get_permalink($idEvento),
            			'foto'            => get_the_post_thumbnail($idEvento, 'agenda-square', array('id' => 'imagen_'.$n, 'class' => 'img-max')),
            			'descripcion'     => get_field('descripcion_corta',$idEvento),
            			'link_entradas'   => get_field('link_entradas',$idEvento),
            			'esEszena'        => $esEszena, #Forzamos para que no muestre el icono
            			'tieneMarra'      => 'si',
            			'posicionEntrada' => 'arriba',
            			'transparencia'   => '',
            			'colorRandom'     => true
                    );
*/
        		} else {
            		echo '*'.$value['idEvento'];
            		continue;
        		}             			
            }
        ?>
                
        
        
        
        
        
        
        
        
    </div><!-- row -->
    </div>
</article>
	
<?php get_footer(); ?> 